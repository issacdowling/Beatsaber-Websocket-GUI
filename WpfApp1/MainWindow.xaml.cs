﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//Extra 3rd party libraries added
using WebSocketSharp;
using Newtonsoft.Json;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
     
        //Class for the window
        public MainWindow()
        {
            InitializeComponent();
            

            //Runs public void Start
            Program.Start();
           //Runs public void update
            update();
        }
        //Call when you want to update the score label
        public void update()
        {
            //Sets Labels
            ScoreLabel.Content = LiveData.S_Score;

            ComboLabel.Content = LiveData.S_Combo;

            AccuracyLabel.Content = "%"+ Math.Round(LiveData.S_Accuracy);

            LNameLabel.Content = MapData.S_SongName;
            if(MapData.S_SongName != null)
            {
                if(MapData.S_SongName.Length > 15)
                {
                    LNameLabel.FontSize = 30;
                }
                else
                {
                    LNameLabel.FontSize = 72;
                }
            }
            

            NJSLabel.Content = MapData.S_NJS;

            BPMLabel.Content = MapData.S_BPM;
            

            if(MapData.S_Difficulty != null)
            {
                DifficultyLabel.Content = MapData.S_Difficulty.Replace("Plus", "+");
            }
           
            

        }

    }

    class LiveData
    {
        //Public list of floating point numbers to record. MUST match 
        //name received from Webserver
        public double Score, Combo, Accuracy;
        public static double S_Score, S_Combo, S_Accuracy;
    }

    class MapData
    {
        //Public list of floating point numbers to record. MUST match 
        //name received from Webserver
        public string SongName, Difficulty, BPM, NJS;
        public static string S_SongName, S_Difficulty, S_BPM, S_NJS;
    }
    class Program
    {

        public static void Start()
        {
            //Create instance of a Websocket Client using websocketsharp

            //Names "live_s" as a Websocket connected to that IP adress
            WebSocket live_ws = new WebSocket("ws://127.0.0.1:2946/BSDataPuller/LiveData");
            live_ws.OnMessage += live_Ws_OnMessage;
            live_ws.Connect();

            //Names "map_s" as a Websocket connected to that IP adress
            WebSocket map_ws = new WebSocket("ws://127.0.0.1:2946/BSDataPuller/MapData");
            map_ws.OnMessage += map_Ws_OnMessage;
            map_ws.Connect();

            
           
          
               
        }




        //Does this when a message is received from live_ws
        public static void live_Ws_OnMessage(object sender, MessageEventArgs le)
        {
            //LD, in the class Livedata, contains the deserialized json
            //coming from the live_ws. So, to acess a specific attribute (say, score),
            //you can write LD.Score.

            //Deserialises the json coming from Websocket and outputs it as LD.xx
            LiveData LD = JsonConvert.DeserializeObject<LiveData>(le.Data);
            //Sets livedata score variable = to LD.score
            LiveData.S_Score = LD.Score;
            //Sets livedata score variable = to LD.Combo
            LiveData.S_Combo = LD.Combo;
            //Sets livedata score variable = to LD.score
            LiveData.S_Accuracy = LD.Accuracy;

            //Updates labels
            Application.Current.Dispatcher.Invoke(new Action(() => { ((MainWindow)System.Windows.Application.Current.MainWindow).update(); }), DispatcherPriority.ContextIdle);
        }
        //Does this when a message is received from map_ws
        public static void map_Ws_OnMessage(object sender, MessageEventArgs me)
        {
            //LD, in the class Livedata, contains the deserialized json
            //coming from the live_ws. So, to acess a specific attribute (say, score),
            //you can write LD.Score.

            //Deserialises the json coming from Websocket and outputs it as LD.xx
            MapData MD = JsonConvert.DeserializeObject<MapData>(me.Data);
            //Sets mapdate songname variable to MD.Songname
            MapData.S_SongName = MD.SongName;
            //Sets mapdata difficulty variable to MD.Difficulty
            MapData.S_Difficulty = MD.Difficulty;
            //Sets mapdata BPM variable = to MD.BOM
            MapData.S_BPM = MD.BPM;
            //Sets mapdate NJS variable = to MD.NJS
            MapData.S_NJS = MD.NJS;

            //Updates labels
            Application.Current.Dispatcher.Invoke(new Action(() => { ((MainWindow)System.Windows.Application.Current.MainWindow).update(); }), DispatcherPriority.ContextIdle);
        }


    }

    //Push Test
}
